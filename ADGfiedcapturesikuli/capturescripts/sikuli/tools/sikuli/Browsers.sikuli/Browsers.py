from sikuli import *
import Util
from Images import *

class Chrome:

    def __init__(self):
        self.chromeActions = Util.Actions()
        if not self.selected():
            self.select()
        self.newWindow()

    def selected(self):
        return exists(Images.chromeOSXHeader)
    
    def select(self):
        doubleClick(Images.chromeIcon)

    def newTab(self):
        click(Images.chromeNewTab)

    def newWindow(self):
        click(Images.chromeFileMenu)
        for i in range(2):
            type(Key.DOWN)
        type(Key.ENTER)

    def visitPage(self,address):
        type(Images.chromeAddressBar, address)
        type(Key.ENTER)

    def loginToJira(self,servername,username,password):
        if not exists(Images.chromeAddressBar):
            self.newWindow()
        type(Images.chromeAddressBar, servername + Key.ENTER)
        if not exists(Images.jiraAdministration):
            wait(Images.jiraLoginPassword)
            self.chromeActions.selectAllAndType(Images.jiraLoginUsername,username)
            type(Key.TAB)
            self.chromeActions.selectAllAndType(Images.jiraLoginPassword,password)
            type(Key.ENTER)

    def openBonfire(self):
        click(Images.bonfireChromeIcon)

    def teardown(self):
        
        #click(Images.jiraProfile)
        #wait(Images.jiraLogout,240)
        #click(Images.jiraLogout)
        click(Images.chromeCloseTabButton)
        
    def installBonfireAddon(self,address):
        type(Images.chromeAddressBar, address + Key.ENTER)
        click(Images.loadExtension)
        click(Images.extensionFolder)
        type(Key.ENTER)
        if exists(Images.bonfireChromeIcon):
            pass
        
    def uninstallBonfireAddon(self):
        find(Images.jiraCapture)
        if exists(Images.jiraCapture):
            rightClick(Images.jiraCapture)
            click(Images.uninstallButton)
            type(Key.ENTER)
            

class Firefox:
    def __init__(self):
        self.firefoxActions = Util.Actions()
        if not self.selected():
            self.select()
        #self.newWindow()

    def select(self):
        doubleClick(Images.firefoxIcon)
            
    def selected(self):
        return exists(Images.chromeOSXHeader)

    def newTab(self):
        
        click(Images.firefoxNewTab)

    #def newWindow(self):
        #click(Images.firefoxFileMenu)
        #type(KeyModifier.n, KeyModifier.CTRL)

    def visitPage(self,address):
        type(Images.firefoxAddressBar, address)
        
        type(Key.ENTER)

    def loginToJira(self,servername,username,password):
        
        type(Images.firefoxAddressBar, servername + Key.ENTER)
        if not exists(Images.jiraAdministration):
            wait(Images.jiraLoginPassword)
            self.firefoxActions.selectAllAndType(Images.jiraLoginUsername,username)
            type(Key.TAB)
            self.firefoxActions.selectAllAndType(Images.jiraLoginPassword,password)
            type(Key.ENTER)

    def openBonfire(self):
        click(Images.bonfireFirefoxIcon)
    
    def shutdownBonfire(self):
        click(Images.closeBonfireIcon)

    def teardown(self):
        click(Images.chromeCloseTabButton)
        #shutdownBonfire()
        

class IE8:
    def __init__(self):
        self.ie8Actions = Util.Actions()
        if not self.selected():
            self.select()

    def select(self):
        click(Images.ie8Icon)

    def selected(self):
        return exists(Images.chromeOSXHeader)

    #def visitPage(self,address):
        type(Images.ie8Addressbar,address)
        type(Key.ENTER)

    def loginToJira(self,servername,username,password):
        wait(Images.ie8AddressBar)
        type(Images.ie8AddressBar, servername + Key.ENTER)
        if not exists(Images.jiraAdministration):
            wait(Images.jiraLoginPasswordie8)
            self.ie8Actions.selectAllAndType(Images.jiraLoginUsernameie8,username)
            type(Key.TAB)
            #wait(Images.jiraLoginPasswordie8)
            self.ie8Actions.selectAllAndType(Images.jiraLoginPasswordie8,password)
            type(Key.ENTER)
    
    def teardown(self):
        click(Images.IEclosebutton)
        
class Safari:
    def __init__(self):
        self.safariActions = Util.Actions()
        if not self.selected():
            self.select()
            
    def select(self):
        click(Images.safariIcon)
    def selected(self):
        return exists(Images.chromeOSXHeader)
    
    def loginToJira(self,servername,username,password):
        wait(Images.safariAddressBar)
        type(Images.safariAddressBar, servername + Key.ENTER)
        wait(Images.jiraLoginUsername,15)
        if not exists(Images.jiraAdministration):
            
            self.safariActions.selectAllAndType(Images.jiraLoginUsername,username)
            type(Key.TAB)
            wait(Images.jiraLoginPassword)
            self.safariActions.selectAllAndType(Images.jiraLoginPassword,password)
            type(Key.ENTER)