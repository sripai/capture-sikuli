from sikuli.Sikuli import *

class Images:

    #Chrome images=
    chromeAddressBar = "chromeAddressBar.png"
    chromeFileMenu = "chromeFileMenu.png"
    chromeIcon = "chromeIcon.png"    #OSX Chrome App Icon
    chromeNewTab = "chromeNewTab.png"
    chromeNewWindow = "chromeNewWindow.png"
    chromeOSXHeader = "chromeOSXHeader.png"    #OSX Chrome Application Menu
    chromeCloseTabButton = "chromeTabCloseButton.png"
    chromeCloseTabButton2 = "chromeCloseTabButton2.png"
    
    #Bonfire images
    bonfireChromeIcon = "bonfireChromeIcon.png"
    bonfireChromeOpenTab = Pattern("bonfireChromeOpenTab.png").targetOffset(87,-4)
    
    bonfireLoginView = Pattern("bonfireLoginView.png").similar(0.84)
    bonfireLoginServername = Pattern("bonfireLoginServername.png").targetOffset(65,0)
    bonfireLoginUsername = Pattern("bonfireLoginUsername.png").targetOffset(65,0)
    bonfireLoginPassword = Pattern("bonfireLoginPassword.png").targetOffset(65,0)
    bonfireLoginSaveButton = "bonfireLoginSaveButton.png"

    bonfireStartingView = "bonfireStartingView.png"
    bonfireSessionsTab = "bonfireSessionsTab.png"
    bonfireIssuesTab = "bonfireIssuesTab.png"
    bonfireTemplatesTab = "bonfireTemplatesTab.png"

    #JIRA images
    jiraAdministration = "jiraAdministration.png"
    jiraLoginButton = "jiraLoginButton.png"
    jiraLoginUsername = "jiraLoginUsername.png"
    jiraLoginPassword = "jiraLoginPassword.png"

    #Miscellaneous Images
    asciiTable = "GOD1IIIDHGCD.png"
	
	#Firefox Images
	firefoxAddressBar = "firefoxAddressBar.png"
    firefoxFileMenu = "firefoxFileMenu.png"
    firefoxIcon = "firefoxIcon.png"    #OSX Chrome App Icon
    firefoxNewTab = "firefoxNewTab.png"
    firefoxNewWindow = "firefoxNewWindow.png"
    #chromeOSXHeader = "chromeOSXHeader.png"    #OSX Chrome Application Menu
    firefoxCloseTabButton = "firefoxTabCloseButton.png"
    firefoxCloseTabButton2 = "firefoxCloseTabButton2.png"
    
    