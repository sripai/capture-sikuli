from sikuli import *
import os
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)

import Bonfire
import Browsers

firefox = Browsers.Firefox()

def testTabSelection():
    b = Bonfire.Bonfire(firefox)
    b.loginToJira("https://jiraforsoftware.jira-dev.com/login?","spai","password123")
    b.loginToBonfire("https://jiraforsoftware.jira-dev.com/","spai","password123")

    b.clickSessionsTab()
    b.clickTemplatesTab()
    b.clickIssuesTab()
    b.teardown()
        
testTabSelection()