from sikuli import *
import os
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)

import Bonfire
import Browsers

safari = Browsers.Safari()

def testTabSelection():
    b = Bonfire.Bonfire(safari)
    b.loginToJira("https://capturetest.jira-dev.com/login?","sysadmin","sysadmin")
    b.loginToBonfire("https://capturetest.jira-dev.com","sysadmin","sysadmin")

    b.clickSessionsTab()
    b.clickTemplatesTab()
    b.clickIssuesTab()
    #b.teardown();
        
testTabSelection()