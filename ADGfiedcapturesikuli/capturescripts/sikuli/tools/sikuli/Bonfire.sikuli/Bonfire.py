from sikuli import *
from Images import *

import Util
import Browsers

class Bonfire:
            
    #CONSTANTS
    #URL for asciitable (for use with annotations)
    ASCIITABLE = "http://ascii-table.com/codepage.php?819"

    def __init__(self,browser):
        self.bonfireActions = Util.Actions()
        self.browser = browser
        #self.startBonfire()

    def startBonfire(self):
        self.openNewTerminal()
        self.startupBonfire()
        self.openBrowser()

    def openBrowser(self):
        if not self.browser.selected():
            self.browser.select()

    def openNewTerminal(self):
        if not exists(Images.bonfireTerminalStartMessage):
            if not exists(Images.bonfireTerminalMenu):
                click(Images.terminalIcon)
            click(Images.bonfireTerminalShell)
            click(Images.bonfireTerminalNewWindow)

    def startupBonfire(self):

        #if bon-debug isn't started
        if not exists(Images.bonfireTerminalStartMessage):
            type("cd /Users/arthur/src/atlassian/bonfire" + Key.ENTER)
            type("hg pull" + Key.ENTER)
            wait(10)
            type("hg up" + Key.ENTER)
            wait(5)
            type("mvn clean install -Dmaven.test.skip=true" + Key.ENTER)
            wait(Images.bonfireTerminalBuildSuccessful, 120)
            type("./tools/scripts/bon-debug -c -Denforcer.skip=true" + Key.ENTER)
            wait(Images.bonfireTerminalStartMessage, 240)

    def shutdownBonfire(self):
        if not exists(Images.bonfireTerminalMenu):
            click(Images.terminalIcon)

        # type("c", KeyModifier.CTRL)
        # click(Images.bonfireTerminalCloseButton)
        
        # if exists(Images.bonfireTerminalConfirmClose):
            # click(Images.bonfireTerminalConfirmClose)
        else:
            pass
        
    def openBonfire(self):
        #if not (self.browser.selected()):
            #self.browser.select()

        #browserType = self.browser.__class__
        if exists(Images.bonfireChromeIcon):
            click(Images.bonfireChromeIcon)
        elif exists(Images.ie8ViewMenu):
            click(Images.ie8ViewMenu)
            type(Key.DOWN)
            type(Key.DOWN)
            type(Key.DOWN)
            type(Key.ENTER)
            for i in range(0,3):
                type(Key.DOWN)
            type(Key.ENTER)
        else :
            click(Images.bonfireSafariIcon)
        
    def loginToBonfire(self,servername,username,password):
        self.openBonfire()
        
        if exists(Images.bonfireIssuesStartingView) or exists(Images.bonfireSessionsStartingView) or exists(Images.bonfireTemplatesStartingView):
            pass
        
        else :
            find(Images.bonfireLoginView)
            self.bonfireActions.selectAllAndType(Images.bonfireLoginServername,servername)
            type(Key.TAB)
            type(username)
            type(Key.TAB)
            type(password)
            type(Key.TAB)
            type(Key.ENTER)
            wait(self.bonfireActions.findStartingView(10), 20)
        
    
    def loginToJira(self,servername,username,password):
        browserClass = self.browser.__class__        #grab the browser object's class
        browserClass.loginToJira(self.browser,servername,username,password)    #go call that object's proper login method
            
    def clickSessionsTab(self):
        click(Images.bonfireSessionsTab)
    
    def clickTemplatesTab(self):
        click(Images.bonfireTemplatesTab)
    
    def clickIssuesTab(self):
        click(Images.bonfireIssuesTab)

    def createIssue(self,summary):
        
        click(Images.bonfireIssueUseTemplateDropdown)
        click(Images.bonfireIssueTemplateMyTemplates)
        self.bonfireActions.findAndClick(Images.bonfireIssueSummaryTextArea)
        self.bonfireActions.selectAllAndType(Images.bonfireIssueSummaryTextArea,summary)
        self.bonfireActions.findAndClick(Images.bonfireCreateIssueButton)

    def toggleFavouritesTwixie(self):
        click(Images.bonfireTemplateFavouritesTwixie)

    def toggleMineTwixie(self):
        click(Images.bonfireTemplateMineTwixie)
        
    def createTemplate(self, templateName):
        wait(Images.bonfireIssuesStartingView)
        click(Images.bonfireTemplatesTab)
        if not exists(Images.bonfireAddTemplateButton, 5):
            click(Images.bonfireTemplateMineTemplateToggle)
        click(Images.bonfireAddTemplateButton)
        type(templateName)
        click(Images.bonfireCreateTemplateButton)

    def viewActiveSession(self):
        if not exists(Images.bonfireActiveSessionBackToTab):
            click(Images.bonfireActiveSession)

    def closeActiveSession(self):
        if exists(Images.bonfireActiveSessionBackToTab):
            click(Images.bonfireActiveSession)
            
    def deleteTemplate(self):
        hover(Images.templateName)
        click(Images.deleteTemplate)
        type(Key.ENTER)
            
    def installBonfireAddon(self,address):
        browserClass = self.browser.__class__        #grab the browser object's class
        browserClass.installBonfireAddon(self.browser,address)    #go call that object's proper login method
        
    def uninstallBonfireAddon(self):
        browserClass = self.browser.__class__        #grab the browser object's class
        browserClass.uninstallBonfireAddon(self.browser)    #go call that object's proper login method

    def teardown(self):
        #self.shutdownBonfire()
        self.browser.teardown()
        
    def createSession(self,sessionName):
        click(Images.bonfireSessionsTab)
        click(Images.bonfireCreateSessionButton)
        self.bonfireActions.findAndClick(Images.bonfireSessionTextArea)
        self.bonfireActions.selectAllAndType(Images.bonfireSessionTextArea,sessionName)
        self.bonfireActions.findAndClick(Images.bonfireCreateSessionButton)
    
    def deleteSession(self):
        wait(Images.bonfireActiveStartingView)
        click(Images.testSessionName)
        click(Images.sessionDelete)
        type(Key.ENTER)
        
        
    def unzipChromeArchive(self):
        doubleClick(Images.commandLine)
        click(Images.bonfireTerminalStartMessage)
        type('7z x "C:\Users\sripai\Desktop\/atlassian-bonfire-2.8.5-SNAPSHOT.zip" -o"C:\Users\sripai\Desktop\/atlassian-bonfire-2.8.5-SNAPSHOT" -y' + Key.ENTER)
        type('exit' + Key.ENTER)

