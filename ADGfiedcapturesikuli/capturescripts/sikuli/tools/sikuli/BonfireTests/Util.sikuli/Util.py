from sikuli import *
from Images import *

class Actions:

    def tripleClick(self,PSMRL):
        hover(PSMRL)
        for x in xrange(3):
            mouseDown(Button.LEFT)
            mouseUp()

    def selectAllAndType(self,image,text):
        self.tripleClick(image)
        type(text)

    #for the use case of having to scroll within the bonfire tab to find an image
    def findAndClick(self,image):
        #to assign focus to the proper frame
        click(Images.bonfireScrollBar)
        for i in range(3):
            if exists(image):
                click(image)
                break
            else:
                type(Key.PAGE_DOWN)

    def findStartingView(self,timeout):
        if exists(Images.bonfireIssuesStartingView, timeout):
            return Images.bonfireIssuesStartingView
        elif exists(Images.bonfireSessionsStartingView, timeout):
            return Images.bonfireSessionsStartingView
        elif exists(Images.bonfireTemplatesStartingView, timeout):
            return Images.bonfireTemplatesStartingView
        else:
            raise AssertionException("No starting view found")