from sikuli import *
import os
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)

import Bonfire
import Browsers

chrome = Browsers.Chrome()

def testTabSelection():
    b = Bonfire.Bonfire(chrome)
    b.loginToJira("http://stone:2990/jira","admin","admin")
    b.loginToBonfire("http://stone.dyn.syd.atlassian.com:2990/jira","admin","admin")

    b.clickSessionsTab()
    b.clickTemplatesTab()
    b.clickIssuesTab()
    b.teardown();
        
testTabSelection()