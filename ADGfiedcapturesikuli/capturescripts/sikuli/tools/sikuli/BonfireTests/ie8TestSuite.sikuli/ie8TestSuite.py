from sikuli import *
import os
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)

from Browsers import IE8
import TestSuite

#create a browser
ie8 = IE8()

#link the browser to the test suite
testSuite = TestSuite.TestSuite(ie8)

#run the tests with the linked browser
try:
    testSuite.testTabSelection()
    testSuite.testCreateIssue()
    testSuite.testCreateTemplate()
    testSuite.testActiveSessionToggle()
except:
    testSuite.errorCode = 1
finally:
    testSuite.teardown()