from sikuli.Sikuli import *

class Images:

    #Starting Bonfire images
    
    terminalIcon = "terminalIcon.png"
    bonfireTerminalMenu = "bonfireTerminal.png"
    bonfireTerminalShell = "bonfireTerminalShell.png"
    bonfireTerminalNewWindow = Pattern("bonfireTerminalNewWindow.png").targetOffset(-89,0)
    
    bonfireTerminalCloseButton = Pattern("bonfireTerminalCloseButton.png").similar(0.66).targetOffset(-119,1)
    bonfireTerminalConfirmClose = Pattern("bonfireTerminalConfirmClose.png").targetOffset(151,36)
    bonfireTerminalHGPulled = "bonfireTerminalHGPulled.png"
    bonfireTerminalHGPullNoChange = "bonfireTerminalHGPullNoChange.png"
    bonfireTerminalStartMessage = Pattern("bonfireTerminalStartMessage.png").similar(0.55)

    #IE8 images
    ie8AddressBar = "ie8AddressBar.png"
    ie8AddressBarSelected = Pattern("ie8AddressBarSelected.png").targetOffset(45,0)
    ie8AddressBarUnselected = "ie8AddressBarUnselected-1.png"
    ie8FileMenu = "ie8FileMenu.png"
    ie8ViewMenu = "ie8ViewMenu.png"
    ie8Icon = Pattern("ie8Icon-2.png").similar(0.81)
    ie8IconSelected = Pattern("ie8IconSelected-1.png").similar(0.90)
    ie8CloseButton = "ie8CloseButton.png"
    ie8NewTab = Pattern("ie8NewTab-1.png").similar(0.76)
    ie8NewWindow = Pattern("ie8NewWindow-1.png").similar(0.95)
    ie8ExplorerBarMenu = Pattern("ie8ExplorerBarMenu.png").similar(0.88).targetOffset(-92,1)
    ie8VirtualMachineIcon = "ie8VirtualMachineIcon.png"
    ie8VirtualMachineMenu = "ie8VirtualMachineMenu.png"
    ie8StartingScreen = Pattern("ie8StartingScreen.png").similar(0.69)

    #Chrome images
    chromeAddressBar = "chromeAddressBar.png"
    chromeFileMenu = "chromeFileMenu.png"
    chromeIcon = Pattern("chromeIcon.png").similar(0.53)    #OSX Chrome App Icon
    chromeNewTab = "chromeNewTab.png"
    chromeNewWindow = "chromeNewWindow.png"
    chromeOSXHeader = "chromeOSXHeader.png"    #OSX Chrome Application Menu
    chromeCloseTabButton = Pattern("chromeCloseTabButton.png").similar(0.90)
    chromeCloseTabButton2 = Pattern("chromeCloseTabButton2.png").similar(0.90)
    
    #Bonfire images
    bonfireChromeIcon = "bonfireChromeIcon.png"
    bonfireChromeOpenTab = Pattern("bonfireChromeOpenTab.png").targetOffset(87,-4)
    bonfireIE8Icon = "bonfireIE8Icon.png"
    bonfireCancelLink = "bonfireCancelLink.png"
    

    #Login images
    bonfireLoginView = "bonfireLoginView-1.png"
    bonfireLoginServername = Pattern("bonfireLoginServername.png").targetOffset(65,0)
    bonfireLoginUsername = Pattern("bonfireLoginUsername.png").targetOffset(65,0)
    bonfireLoginPassword = Pattern("bonfireLoginPassword.png").targetOffset(65,0)
    bonfireLoginSaveButton = "bonfireLoginSaveButton.png"

    #Tabs images
    bonfireSessionsStartingView = Pattern("bonfireSessionsStartingView.png").similar(0.69)
    bonfireSessionsTab = "bonfireSessionsTab.png"
    bonfireSessionsTabSelected = Pattern("bonfireSessionsTabSelected.png").similar(0.91).targetOffset(0,7)
    bonfireIssuesStartingView = "bonfireIssuesStartingView.png"
    bonfireIssuesTab = "bonfireIssuesTab.png"
    bonfireIssuesTabSelected = Pattern("bonfireIssuesTabSelected-1.png").similar(0.90).targetOffset(0,9)
    bonfireTemplatesStartingView = "bonfireTemplatesStartingView.png"
    bonfireTemplatesTab = "bonfireTemplatesTab.png"
    bonfireTemplatesTabSelected = Pattern("bonfireTemplatesTabSelected.png").similar(0.95).targetOffset(0,9)

    #Issue Tab images

    bonfireCreateIssueButton = "bonfireCreateIssueButton.png"
    bonfireIssueProjectDropdown = Pattern("bonfireIssueProjectDropdown.png").targetOffset(102,3)
    bonfireIssueIssueTypeDropdown = Pattern("bonfireIssueIssueTypeDropdown.png").targetOffset(83,0)
    bonfireIssuePriorityDropdown = Pattern("bonfireIssuePriorityDropdown.png").targetOffset(91,-1)
    bonfireIssueSuccessMessage = Pattern("bonfireIssueSuccessMessage.png").similar(0.62)
    bonfireIssueSummaryTextArea = Pattern("bonfireIssueSummaryTextArea.png").similar(0.59).targetOffset(31,23)
    bonfireIssueTemplateMyTemplates = Pattern("bonfireIssueTemplateMyTemplates.png").similar(0.89).targetOffset(0,16)
    bonfireIssueUseTemplateDropdown = Pattern("bonfireIssueUseTemplateDropdown.png").targetOffset(78,0)
    

    #Templates Tab images
    bonfireAddTemplateButton = "bonfireAddTemplateButton.png"
    bonfireCreateTemplateButton = "bonfireCreateTemplateButton.png"
    bonfireTemplateAddVariableButton = "bonfireTemplateAddVariableButton.png"
    bonfireTemplateCreateVariableButton = "bonfireTemplateCreateVariableButton.png"
    bonfireTemplateFavouritesTwixie = "bonfireTemplateFavouritesTwixie.png"
    bonfireTemplateMineTwixie = "bonfireTemplateMineTwixie.png"
    bonfireTemplateMineTemplateToggle = "bonfireTemplateMineTemplatesToggle.png"
    bonfireTemplateMineVariablesToggle = "bonfireTemplateMineVariablesToggle.png"
    bonfireTemplateVariablesName = Pattern("bonfireTemplateVariablesName.png").targetOffset(79,0)
    bonfireTemplateVariablesSelector = Pattern("bonfireTemplateVariablesSelector.png").targetOffset(91,0)
    bonfireTemplateItemHoverIcons = "bonfireTemplateItemHoverIcons.png"
    bonfireTemplateItemTemplate1 = Pattern("bonfireTemplateItemTemplate1.png").similar(0.98)
    madeATemplate = Pattern("madeATemplate.png").similar(0.78)
    

    #JIRA images
    jiraAdministration = Pattern("jiraAdministration.png").similar(0.80)
    jiraLoginButton = "jiraLoginButton.png"
    jiraLoginUsername = Pattern("jiraLoginUsername.png").similar(0.60)
    jiraLoginPassword = Pattern("jiraLoginPassword.png").similar(0.67)

    #Miscellaneous Images
    bonfireActiveSession = "bonfireActiveSession.png"
    bonfireActiveSessionBackToTab = Pattern("bonfireActiveSessionBackToTab.png").targetOffset(-26,0)
    bonfireTemplateVariableItemTest = "bonfireTemplateVariableItemTest-1.png"
    bonfireScrollBar = Pattern("bonfireScrollBar.png").similar(0.85).targetOffset(-2,-33)
    