from sikuli import *
import Util
from Images import *

class Chrome:

    def __init__(self):
        self.chromeActions = Util.Actions()
        if not self.selected():
            self.select()
        self.newWindow()

    def selected(self):
        return exists(Images.chromeOSXHeader)
    
    def select(self):
        click(Images.chromeIcon)

    def newTab(self):
        click(Images.chromeNewTab)

    def newWindow(self):
        click(Images.chromeFileMenu)
        click(Images.chromeNewWindow)

    def visitPage(self,address):
        type(Images.chromeAddressBar, address)
        type(Key.ENTER)

    def loginToJira(self,servername,username,password):
        
        if not exists(Images.chromeAddressBar):
            self.newWindow()
        type(Images.chromeAddressBar, servername + Key.ENTER)
        if not exists(Images.jiraAdministration):
            wait(Images.jiraLoginPassword)
            self.chromeActions.selectAllAndType(Images.jiraLoginUsername,username)
            type(Key.TAB)
            self.chromeActions.selectAllAndType(Images.jiraLoginPassword,password)
            type(Key.ENTER)

    def openBonfire(self):
        click(Images.bonfireChomeIcon)
        
    def installBonfireAddon(self,address):
        type(Images.chromeAddressBar, address + Key.ENTER)
        
        

    def teardown(self):
        for x in xrange(2):
            if(exists(Images.chromeCloseTabButton)):
                click(Images.chromeCloseTabButton)
            elif(exists(Images.chromeCloseTabButton2)):
                click(Images.chromeCloseTabButton2)

class IE8:
    def __init__(self):
        self.IE8Actions = Util.Actions()

    def select(self):
        if not exists(Images.ie8VirtualMachineMenu):
            click(Images.ie8VirtualMachineIcon)
            
        if not exists(Images.ie8IconSelected):
            click(Images.ie8Icon)
            
    def selected(self):
        return (exists(Images.ie8IconSelected) and exists(Images.ie8VirtualMachineMenu))

    def newTab(self):
        click(Images.ie8FileMenu)
        click(Images.ie8NewTab)

    def newWindow(self):
        click(Images.ie8FileMenu)
        click(Images.ie8NewWindow)

    def visitPage(self,address):
        self.selectAddressBar()
        type(address)
        type(Key.ENTER)

    def loginToJira(self,servername,username,password):
        self.visitPage(servername)
        
        if not exists(Images.jiraAdministration, 20):
            wait(Images.jiraLoginPassword, 20)
            type(username)
            type(Key.TAB)
            type(password)
            type(Key.ENTER)

    def openBonfire(self):
        if not( exists(Images.bonfireIssuesStartingView) or exists(Images.bonfireLoginView)):
            click(Images.ie8ViewMenu)
            hover(Images.ie8ExplorerBarMenu)
            click(Images.bonfireIE8Icon)

    def closeBonfire(self):
        click(Images.ie8ViewMenu)
        hover(Images.ie8ExplorerBarMenu)
        click(Images.bonfireIE8Icon)

    def selectAddressBar(self):
        if exists(Images.ie8AddressBarUnselected, 15):
            click(Images.ie8AddressBar)
        elif exists(Images.ie8AddressBarSelected, 15):
            doubleClick(Images.ie8AddressBarSelected)
            
    def teardown(self):
        if not self.selected():
            self.select()
            self.closeBonfire()
            click(Images.ie8CloseButton)
