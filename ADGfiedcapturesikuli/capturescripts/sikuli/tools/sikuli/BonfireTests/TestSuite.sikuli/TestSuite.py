from sikuli import *
import os
import Bonfire
from Images import *

class TestSuite:
    def __init__(self,browser):
        self.browser = browser

        #will be transferred to Bonfire and Browser during teardown
        self.errorCode = 0
        
        global b
        b = Bonfire.Bonfire(self.browser)
        
        b.loginToJira("http://stone.dyn.syd.atlassian.com:2990/jira","admin","admin")
        b.loginToBonfire("http://stone.dyn.syd.atlassian.com:2990/jira","admin","admin")

    def testTabSelection(self):
        b.clickSessionsTab()
        assert(exists(Images.bonfireSessionsTabSelected))
        b.clickTemplatesTab()
        assert(exists(Images.bonfireTemplatesTabSelected))
        b.clickIssuesTab()
        assert(exists(Images.bonfireIssuesTabSelected))

    def testCreateIssue(self):
        b.createIssue("Pete made Sikuli create this issue, Yo </jessie>")
        assert(exists(Images.bonfireIssueSuccessMessage, 10))

    def testCreateTemplate(self):
        b.createTemplate("Made a template")
        assert(exists(Images.madeATemplate, 5))

    def testActiveSessionToggle(self):
        b.viewActiveSession()
        b.closeActiveSession()
        assert(exists(Images.bonfireTemplatesStartingView))
        
    def teardown(self):
        b.teardown()
        exit(errorCode)