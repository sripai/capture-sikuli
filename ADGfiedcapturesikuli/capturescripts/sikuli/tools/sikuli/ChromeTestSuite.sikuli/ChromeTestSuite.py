from sikuli import *
import os
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)

import Bonfire
import Browsers

chrome = Browsers.Chrome()

def installBonfire():
    a = Bonfire.Bonfire(chrome)
    a.unzipChromeArchive()
    a.installBonfireAddon("chrome://extensions/")
    #a.teardown();
    #b.uninstallBonfireAddon()

    #b.clickSessionsTab()
    #b.clickTemplatesTab()
    #b.clickIssuesTab()
    #b.createIssue("This is a test Summary")
    
installBonfire()

def setUpBonfire():
    b = Bonfire.Bonfire(chrome)
    b.loginToJira("https://jiraforsoftware.jira-dev.com/login?","spai","password123")
    b.loginToBonfire("https://jiraforsoftware.jira-dev.com","spai","password123")

setUpBonfire()

def createIssue():
    c = Bonfire.Bonfire(chrome)
    c.createIssue("This is a test Summary")
createIssue()    
def createTempl():
    d = Bonfire.Bonfire(chrome)
    d.createTemplate("Test template")
    d.deleteTemplate()
    
   
createTempl()
def createSession():
    e = Bonfire.Bonfire(chrome)
    e.createSession("Test template")
    e.deleteSession()
    e.uninstallBonfireAddon()
    e.teardown();

createSession()