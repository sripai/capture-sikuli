#!/bin/bash

printf "Deploying artifacts to stone.sydney.atlassian.com\n"

printf "Current directory is $(pwd) \n"

printf "Contacting Stone \n"
ping -c 1 stone.sydney.atlassian.com

printf "Copying XPI files\n"

scp -i /opt/bamboo-agent/.ssh/isa_dsa browser/extension-xpi/target/*.xpi arthur@stone.sydney.atlassian.com:/Library/WebServer/Documents/firefox-dev/
scp -i /opt/bamboo-agent/.ssh/isa_dsa browser/extension-xpi/target/ff_update.rdf arthur@stone.sydney.atlassian.com:/Library/WebServer/Documents/firefox-dev/

printf "Copying Safari files\n"

ssh -i /opt/bamboo-agent/.ssh/isa_dsa arthur@stone.sydney.atlassian.com 'rm -rf /Library/WebServer/Documents/safari-dev/bonfire.safariextension/'
scp -i /opt/bamboo-agent/.ssh/isa_dsa -r browser/extension-safariextz/target/bonfire.safariextension/ arthur@stone.sydney.atlassian.com:/Library/WebServer/Documents/safari-dev/bonfire.safariextension/
scp -i /opt/bamboo-agent/.ssh/isa_dsa -r browser/extension-safariextz/target/safari_update.plist arthur@stone.sydney.atlassian.com:/Library/WebServer/Documents/safari-dev/safari_update.plist

printf "\n DONE! \n"
