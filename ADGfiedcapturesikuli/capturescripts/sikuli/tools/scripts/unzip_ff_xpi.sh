#/bin/bash

cd "$1"

printf "Current directory is $(pwd) \n"

mv bonfire-dev@atlassian.com.xpi bonfire-dev@atlassian.com.zip

unzip bonfire-dev@atlassian.com.zip -d bonfire-dev@atlassian.com

unzip bonfire-dev@atlassian.com/chrome/bonfire.jar -d bonfire-dev@atlassian.com/chrome/bonfire

echo "content	bonfire	chrome/bonfire/content/
content	bonfire	chrome/bonfire/content/ contentaccessible=yes
locale	bonfire	en-US	chrome/bonfire/locale/en-US/
skin	bonfire	classic/1.0	chrome/bonfire/skin/
overlay	chrome://browser/content/browser.xul	chrome://bonfire/content/firefox/firefoxOverlay.xul" > bonfire-dev@atlassian.com/chrome.manifest