#/bin/bash

PATH=${JAVA_HOME}/bin:${M2_HOME}/bin:$PATH

#
# Overrides since the Bamboo build keys have not been setup on atlassian42.
# Much easier to do this than to actually fix it properly
#
REMOTESERVER=atlassian04.private.atlassian.com
REMOTEUSER=jira-wac-deploy
PRIVATEKEY=/opt/bamboo-agent/.ssh/jira-wac-deploy_rsa
REMOTEDIR=/import/downloads/software/bonfire
DEPLOYDIR=$1

DEPLOY_COMMON_INCLUDE="$(dirname $0)/deploy-common-include.sh"
source $DEPLOY_COMMON_INCLUDE


printf "Bamboo Deploy to DAC script invoked\n"
pwd
ls -ltr

variable_setup

transferToDAC bamboo