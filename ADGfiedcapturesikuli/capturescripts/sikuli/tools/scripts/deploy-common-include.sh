
function usage() {
   echo "Usage : "
   echo "   $(basename $0) -i"
   echo "       -i|--info                       - show the info only - no upload"
   echo "       -h|--help                       - this help"
   exit 99;
}

# translate long options to short
for arg
do
    delim=""
    case "$arg" in
       -help) args="${args}-h " ;;
       --help) args="${args}-h " ;;

       --info) args="${args}-i " ;;
       -info) args="${args}-i " ;;
       # pass through anything else
       *) [[ "${arg:0:1}" == "-" ]] || delim="\""
           args="${args}${delim}${arg}${delim} " ;;
    esac
done
# reset the translated args
eval set -- $args

while getopts ":i" opt; do
  case $opt in
    i) INFOONLY=1 ;;
    \?) usage ;;
    :)
      echo "option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $(($OPTIND - 1))



function variable_setup() {


    if [ -z $REMOTESERVER ] ; then
        REMOTESERVER=downloads.internal.atlassian.com
    fi

    if [ -z $REMOTEUSER ] ; then
        REMOTEUSER=$(whoami)
    fi

    if [ -z $REMOTEDIR ] ; then
        REMOTEDIR=/import/downloads/software/bonfire
    fi

    if [ -z $DEPLOYDIR ] ; then
        DEPLOYDIR=.
    fi

    LATESTDIR=/import/downloads/software/bonfire

    printf "Working out maven versions in play...\n"

    DIRVERSION=$(mvn validate -N | sed -n -e 's/.*extension.version.directory : \(.*\)/\1/p')
    PROJECTVERSION=$(mvn validate -N | sed -n -e 's/.*project.version : \(.*\)/\1/p')

    printf "\n"
    printf "   PROJECTVERSION = ${PROJECTVERSION}\n"
    printf "   DIRVERSION=${DIRVERSION}\n"
    printf "   REMOTESERVER=${REMOTESERVER}\n"
    printf "   REMOTEUSER=${REMOTEUSER}\n"
    printf "   REMOTEDIR=${REMOTEDIR}\n"
    printf "   LATESTDIR=${LATESTDIR}\n"
    printf "   PRIVATEKEY=${PRIVATEKEY}\n"
    printf "\n"
}

function printcmd() {
    printf "   > $1...\n"
}

function remoteCmdNoErrors () {
    printcmd "ssh $1"
    if [ -z ${PRIVATEKEY} ] ; then
        ssh ${REMOTEUSER}@${REMOTESERVER} $1
    else
        ssh -i ${PRIVATEKEY} ${REMOTEUSER}@${REMOTESERVER} $1
    fi
}

function remoteCmd () {
    printcmd "ssh $1"
    if [ -z ${PRIVATEKEY} ] ; then
        ssh ${REMOTEUSER}@${REMOTESERVER} $1
    else
        ssh -i ${PRIVATEKEY} ${REMOTEUSER}@${REMOTESERVER} $1
    fi
    if [ $? != 0 ];then
        echo ">>> FAILURE: Failed to execute remote command - $1"
        exit 1
    fi
}

function scpCmd() {
    printcmd "scp $1 $2"
    if [ -z ${PRIVATEKEY} ] ; then
        scp $1 $2
    else
        scp -i ${PRIVATEKEY} $1 $2
    fi
}

function linkAsLatestOnDAC () {

    local remoteDir=$1
    local fileName=$2
    local latest=$3

    fileName=$(basename ${fileName})
    echo "Removing and softlinking ${remoteDir}/${fileName} as ${latest}"

    # -L means is a symbolic link
    remoteCmd "if [[ -L ${remoteDir}/${latest} ]];then rm ${remoteDir}/${latest} ; fi ; ln -s ${remoteDir}/$fileName ${remoteDir}/${latest}"
}

function scpToDAC () {

    local remoteDir=${REMOTEDIR}/${DIRVERSION}/$1
    local latestDir=${LATESTDIR}/${DIRVERSION}/$1
    local fileName=$2
    local latest=$3
    local baseFileName=$(basename ${fileName})

    echo "INFO: Transferring ${fileName} to ${remoteDir}..."
    scpCmd ${fileName} ${REMOTEUSER}@${REMOTESERVER}:${remoteDir}
    if [ $? != 0 ];then
        echo ">>> FAILURE: Transfer to DAC Failed for ${fileName} to ${remoteDir}"
        exit 1
    fi

    remoteCmdNoErrors "chmod g+w ${remoteDir}/$baseFileName"

    if [ ${latest} ]; then
        linkAsLatestOnDAC ${latestDir} ${fileName} ${latest}
    fi
}

function scpFromStoneToDAC () {

    local fileName=$2
    local stoneFile=arthur@stone.syd.atlassian.com:/Users/arthur/src/atlassian/bonfire/${fileName}

    scpCmd  ${stoneFile} ${fileName}
    if [ $? != 0 ];then
        echo ">>> FAILURE: Transfer from Stone to Local Failed for ${stoneFile} to ${fileName}"
        exit 1
    fi

    scpToDAC $1 $2 $3
}



function makeDACDir () {

    local remoteDir=$1

    echo "INFO: Creating ${remoteDir} directory on DAC if needed..."
    remoteCmd "if [[ ! -d ${remoteDir} ]];then mkdir ${remoteDir} && chmod g+sxw ${remoteDir} ; fi"
}

function transferToDAC () {

    if [[ ! -d ${DEPLOYDIR} ]]; then
       echo ">>> FAILURE: Deploy directory ${DEPLOYDIR} does not exist"
       exit 1;
    fi

    makeDACDir ${REMOTEDIR}/${DIRVERSION}
    makeDACDir ${REMOTEDIR}/${DIRVERSION}/firefox
    makeDACDir ${REMOTEDIR}/${DIRVERSION}/ie
    makeDACDir ${REMOTEDIR}/${DIRVERSION}/safari
    makeDACDir ${REMOTEDIR}/${DIRVERSION}/plugin
    makeDACDir ${REMOTEDIR}/${DIRVERSION}/help

    if [ "$1" == "full" ] ; then

        scpToDAC safari ${DEPLOYDIR}/browser/extension-safariextz/target/atlassian-bonfire-${PROJECTVERSION}.safariextz atlassian-bonfire-latest.safariextz
        scpToDAC safari ${DEPLOYDIR}/browser/extension-safariextz/target/safari_update.plist

        scpToDAC ie ${DEPLOYDIR}/browser/extension-ie/target/atlassian-bonfire-${PROJECTVERSION}.msi atlassian-bonfire-latest.msi

        scpToDAC plugin ${DEPLOYDIR}/plugin/target/atlassian-bonfire-plugin-${PROJECTVERSION}.jar
    fi
    if [ "$1" == "bamboo" ] ; then
        scpToDAC ie ${DEPLOYDIR}/browser/extension-ie/target/atlassian-bonfire-${PROJECTVERSION}.msi atlassian-bonfire-latest.msi
    fi

    scpToDAC firefox ${DEPLOYDIR}/browser/extension-xpi/target/atlassian-bonfire-${PROJECTVERSION}.xpi atlassian-bonfire-latest.xpi
    scpToDAC firefox ${DEPLOYDIR}/browser/extension-xpi/target/ff_update.rdf

}
