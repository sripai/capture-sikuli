#/bin/bash
DEPLOY_COMMON_INCLUDE="deploy-common-include.sh"
source $DEPLOY_COMMON_INCLUDE


function showResultsInfo() {

cat <<End-of-message

    Done transferring files
    __________________________

    ${DAC_SSH}${REMOTEDIR}/${DIRVERSION}/help/

End-of-message

}

function transferHelpToDAC () {

    if [[ ! -d ${DEPLOYDIR} ]]; then
       echo ">>> FAILURE: Deploy directory ${DEPLOYDIR} does not exist"
       exit 1;
    fi

    makeDACDir ${REMOTEDIR}/${DIRVERSION}/help/img

    for file in ${DEPLOYDIR}/browser/extension-core/src/main/resources/chrome/content/help/*; do
        if [ ! -d $file ]; then
            scpToDAC help $file
        fi
    done

    for file in ${DEPLOYDIR}/browser/extension-core/src/main/resources/chrome/content/help/img/*; do
        if [ ! -d $file ]; then
            scpToDAC help/img $file
        fi
    done
}

#### MAIN CODE

printf "Manual Deploy Help Pages to DAC script invoked\n"
pwd

variable_setup


if [[ ${INFOONLY} -eq 0 ]]; then
    transferHelpToDAC
fi
showResultsInfo
