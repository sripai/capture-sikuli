#/bin/bash

SCRIPTS_DIRECTORY="`dirname \"$0\"`"
DEPLOY_COMMON_INCLUDE=$SCRIPTS_DIRECTORY"/deploy-common-include.sh"
source $DEPLOY_COMMON_INCLUDE


function showResultsInfo() {

cat <<End-of-message

    Done transferring artifacts
    __________________________

    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/safari/atlassian-bonfire-${PROJECTVERSION}.safariextz
    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/safari/safari_update.plist

    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/firefox/atlassian-bonfire-${PROJECTVERSION}.xpi
    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/firefox/ff_update.rdf

    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/ie/atlassian-bonfire-${PROJECTVERSION}.msi

    ${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/plugin/atlassian-bonfire-plugin-${PROJECTVERSION}.jar


    The direct download links are
    _____________________________


    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/safari/atlassian-bonfire-${PROJECTVERSION}.safariextz
    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/firefox/atlassian-bonfire-${PROJECTVERSION}.xpi
    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/ie/atlassian-bonfire-${PROJECTVERSION}.msi

    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/safari/atlassian-bonfire-latest.safariextz
    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/firefox/atlassian-bonfire-latest.xpi
    http://downloads.atlassian.com/software/bonfire/${DIRVERSION}/ie/atlassian-bonfire-latest.msi

    For chrome extension you need to manually update it to the Chrome Web Store


    The ADM request proforma text. Need an issue for JAC and an issue for EACJ
    _____________________________


Can we please update this system to the latest Bonfire release.

Bonfire has been tested on the respective QA systems and is ready and rip roaring to go.

The plugin files can be found at:

	${REMOTESERVER}${REMOTEDIR}/${DIRVERSION}/plugin/atlassian-bonfire-plugin-${PROJECTVERSION}.jar

Since the systems are JIRA 5.x they do NOT need a system restart to run plugin upgrade tasks and hence should JUST WORK™.

Thanks in advance from the Bonfire team.

    _____________________________

Also tell someone in support to add an additional version to the Bonfire SAC project

End-of-message

}

#### MAIN CODE

printf "Manual Deploy to DAC script invoked\n"

variable_setup

if [[ ${INFOONLY} -eq 0 ]]; then
    transferToDAC full
fi
showResultsInfo
