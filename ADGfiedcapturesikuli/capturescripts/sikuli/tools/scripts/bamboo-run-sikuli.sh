#!/bin/bash

cd /Users/arthur/src/atlassian/bonfire
pwd
/Users/arthur/src/atlassian/bonfire/tools/scripts/bon-debug > /tmp/sikuli.$$ 2>&1 &
BON_DEBUG_PID=$!

#tail --pid $BON_DEBUG_PID -n +0 -f /tmp/sikuli.$$ | grep -q "jira started successfully"

FAILED=0
while ! grep -q "jira started successfully" /tmp/sikuli.$$
do
  sleep 5
  date
  if ! kill -0 $BON_DEBUG_PID
  then
    echo failed - process exited
    FAILED=1
    break
  fi
done

echo FAILED: $FAILED

if [[ $FAILED == 0 ]]
then
  /Applications/sikuli-ide.app/sikuli-ide.sh -r /Users/arthur/Desktop/BonfireTests/ie8TestSuite.sikuli
  TESTRESULT=$?
else
  TESTRESULT=1
  echo "bon-debug did not successfully start" >&2
fi

kill -KILL $BON_DEBUG_PID
exit $TESTRESULT
