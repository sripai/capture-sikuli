echo The following environment variables are in play
set

set WindowsSdkPath=%Programfiles%\Microsoft SDKs\Windows\v7.1
set MSBuildPath=%windir%\Microsoft.NET\Framework64\v4.0.30319
set PATH=%WindowsSdkPath%\Bin;%MSBuildPath%;%PATH%

echo The following environment variables are NOW in play
set
dir

call "%WindowsSdkPath%\Bin\Setenv.cmd"

mvn clean install --projects browser/extension-ie -Dmaven.test.skip=true --also-make -Dci.build.number=%1